package me.Kostronor.VIP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import me.Kostronor.VIP.Commands.VIPCommand;
import me.Kostronor.VIP.Database.MySQL;
import me.Kostronor.VIP.Permissions.VIPPermissions;
import me.Kostronor.VIP.events.VIPPlayerListener;
import me.Kostronor.VIP.metrics.Metrics;
import me.Kostronor.VIP.metrics.Metrics.Graph;
import me.Kostronor.VIP.updater.Updater;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * VIP for Bukkit
 * 
 * @author Kostronor
 */

public class VIP extends JavaPlugin {

	// Links the VIPPlayerListener
	private final VIPPlayerListener playerListener = new VIPPlayerListener(this);
	public static PluginDescriptionFile pdfFile = null;
	private FileConfiguration configuration;
	public VIPPermissions permissions;
	private MySQL DB;
	private static VIP plugin;
	private boolean mysql = false;
	public YamlConfiguration logintimes;
	public File loginfile;
	private static boolean verbose;
	private static boolean debug;

	@Override
	public void onDisable() {
		getLogger().info("[VIP] Disabled");
	}

	@Override
	public void onEnable() {
		VIP.plugin = this;
		// File configfile = new File(plugin.getDataFolder(), "config.yml");
		// if (!configfile.exists()) {
		// configfile.getParentFile().mkdirs();
		// getConfig().saveDefaultConfig();
		// }
		getConfig().options().copyDefaults(true);
		this.saveDefaultConfig();
		this.configuration = getConfig();
		pdfFile = this.getDescription();
		debug = this.configuration.getBoolean("debug", false);
		verbose = (this.configuration.getBoolean("verbose", false) || debug);
		firstRun();

		permissions = new VIPPermissions(this);

		// Create the pluginmanager
		PluginManager pm = getServer().getPluginManager();
		// Create listeners
		pm.registerEvents(playerListener, this);

		getCommand("vip").setExecutor(new VIPCommand(this));

		// Get the information from the yml file.
		PluginDescriptionFile pdfFile = this.getDescription();

		// Print that the plugin has been enabled!
		getLogger().info(pdfFile.getName() + " version " + pdfFile.getVersion() + " by Kostronor is enabled!");
		if (configuration.getBoolean("Use MySQL", false)) {
			DB = new MySQL(this);
			this.mysql = true;
		}
		if (this.configuration.getBoolean("auto-update", true)) {
			new Updater(this, 35502, this.getFile(), Updater.UpdateType.DEFAULT, false);
		}
		if (this.configuration.getBoolean("enable-metrics", true)) {
			enableMetrics();
		}
	}

	/**
	 * called once by enablePlugin when metrics is enabled!
	 */
	public void enableMetrics() {
		final boolean usemysql = this.mysql;
		getServer().getScheduler().runTaskLater(this, new Runnable() {

			@Override
			public void run() {

				try {
					Metrics metrics = new Metrics(VIP.plugin);
					String database = "SQLite";
					if (usemysql) {
						database = "MySQL";
					}
					Graph databaseGraph = metrics.createGraph("Database Engine");
					databaseGraph.addPlotter(new Metrics.Plotter(database) {
						@Override
						public int getValue() {
							return 1;
						}
					});
					metrics.start();
				} catch (Exception e) {
					// Failed to submit the stats :-(
				}

			}
		}, 20 * 60);
	}

	/**
	 * Creating additional yml files if not existing. Loading these files.
	 */
	public void firstRun() {
		loginfile = new File(plugin.getDataFolder(), "logintimes.yml");
		if (!loginfile.exists()) {
			loginfile.getParentFile().mkdirs();
			copy(plugin.getResource("logintimes.yml"), loginfile);
		}
		logintimes = new YamlConfiguration();
		loadYamls();
	}

	/**
	 * Loading locale.yml suppressing exceptions on System.err
	 */
	public void loadYamls() {
		try {
			logintimes.load(loginfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Copies content from Inputstrem to File.
	 * 
	 * @param in
	 *            Stream to read from
	 * @param file
	 *            File to copy to
	 */
	private void copy(final InputStream in, final File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FileConfiguration getConfigs() {
		return configuration;
	}

	public MySQL getDB() {
		return DB;
	}

	/**
	 * logs verbose output
	 * 
	 * @param text
	 *            the logged text
	 */
	public void logInfo(final String text) {
		if (verbose) {
			plugin.getLogger().info(text);
		}
	}

	/**
	 * logs debug output
	 * 
	 * @param text
	 *            the logged text
	 */
	public static void logDebug(final String text) {
		if (debug) {
			plugin.getLogger().info(text);
		}
	}
}
