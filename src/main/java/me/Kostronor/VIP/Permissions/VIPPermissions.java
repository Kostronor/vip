package me.Kostronor.VIP.Permissions;

import java.sql.SQLException;

import me.Kostronor.VIP.VIP;

import org.bukkit.entity.Player;

public class VIPPermissions {
	private final VIP plugin;

	public VIPPermissions(VIP plugin) {
		this.plugin=plugin;
	}

	public boolean canAddPlayer(Player player){	
		if(player != null){
			return player.hasPermission("VIP.add");
		}
		return false;
	}
	public boolean canRemovePlayer(Player player){	
		if(player != null){
			return player.hasPermission("VIP.remove");
		}
		return false;
	}
	public boolean canEnable(Player player){	
		if(player != null){
			return player.hasPermission("VIP.enable");
		}
		return false;
	}
	public boolean canDisable(Player player){
		if(player != null){
			return player.hasPermission("VIP.disable");
		}
		return false;
	}
	public boolean canToggleKick(Player player){
		if(player != null){
			return player.hasPermission("VIP.toggleKick");
		}
		return false;
	}
	public int isVIP(Player player){
		String playerName = player.getName();
		if(plugin.getConfig().getBoolean("Use MySQL", false)){
			if (plugin.getConfig().getBoolean("Use permissions for VIP list", false)){
				try {
					return plugin.getDB().getPermission(playerName);
				} catch (SQLException e) {
					plugin.getLogger().warning("SQL-Error: " + e.getMessage());
					return 0;
				}
			}else{
				try {
					return plugin.getDB().getPlayer(playerName);
				} catch (SQLException e) {
					plugin.getLogger().warning("SQL-Error: " + e.getMessage());
					return 0;
				}

			}
		}else{
			if (plugin.getConfig().getBoolean("Use permissions for VIP list", false)){
				if(player.hasPermission("VIP.10")){
					return 10;
				}
				if(player.hasPermission("VIP.9")){
					return 9;
				}
				if(player.hasPermission("VIP.8")){
					return 8;
				}
				if(player.hasPermission("VIP.7")){
					return 7;
				}
				if(player.hasPermission("VIP.6")){
					return 6;
				}
				if(player.hasPermission("VIP.5")){
					return 5;
				}
				if(player.hasPermission("VIP.4")){
					return 4;
				}
				if(player.hasPermission("VIP.3")){
					return 3;
				}
				if(player.hasPermission("VIP.2")){
					return 2;
				}
				if(player.hasPermission("VIP.1")){
					return 1;
				}
				if(player.hasPermission("VIP.VIP")){
					return 1;
				}
				return 0;
			}
			else{
				if(plugin.getConfig().getString("VIPs." + playerName, null)==null){
					return 0;
				}
				return plugin.getConfig().getInt("VIPs." + playerName, 0);
			}

		}
	}
}