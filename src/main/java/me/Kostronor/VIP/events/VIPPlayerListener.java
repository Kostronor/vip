// Package
package me.Kostronor.VIP.events;

// Imports

import java.io.IOException;
import java.util.LinkedList;

import me.Kostronor.VIP.VIP;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class VIPPlayerListener implements Listener {

	public static VIP plugin;
	public String callingPlayerName;

	public VIPPlayerListener(final VIP instance) {
		plugin = instance;
	}

	@EventHandler
	public void onPlayerLogin(final PlayerLoginEvent event) {
		if (plugin.getServer().getOnlinePlayers().length < plugin.getServer().getMaxPlayers()) {
			VIP.logDebug("VIP_DEBUG free space");
			return;
		}
		if (event.getResult().equals(Result.KICK_BANNED) || event.getResult().equals(Result.KICK_WHITELIST)) {
			VIP.logDebug("VIP_DEBUG banned|whitelist");
			return;
		}
		Player player = event.getPlayer();
		if (plugin.permissions.isVIP(player) == 0) {
			VIP.logDebug("VIP_DEBUG VIP_0");
			event.setResult(Result.KICK_FULL);
			return;
		} else {
			VIP.logDebug("VIP_DEBUG VIP_" + plugin.permissions.isVIP(player));
			if (plugin.getConfigs().getBoolean("no kick for vip", false)) {
				event.setResult(Result.ALLOWED);
				return;
			}
		}
		Player online[] = plugin.getServer().getOnlinePlayers();
		LinkedList<Player> options = new LinkedList<Player>();
		int it = 0;
		int p = 0;
		while ((it == 0) && (p < plugin.permissions.isVIP(player))) {
			for (int i = 0; i < online.length; i++) {
				if (plugin.permissions.isVIP(online[i]) == p) {
					options.add(online[i]);
					it++;
				}
			}
			p++;
		}
		if (it == 0) {
			event.setResult(Result.KICK_FULL);
			return;
		}
		if (it == 1) {
			options.get(0).kickPlayer(plugin.getConfigs().getString("custom kick message", "Server is full. A VIP signed in."));
			event.setResult(Result.ALLOWED);
			return;
		}
		int kick = 0;
		for (int i = 1; i < options.size(); i++) {
			int iLoginTime = plugin.logintimes.getInt("DO NOT EDIT -- Login times." + options.get(i).getName(), 0);
			int kickLoginTime = plugin.logintimes.getInt("DO NOT EDIT -- Login times." + options.get(kick).getName(), 0);
			if ((iLoginTime != 0) && (iLoginTime > kickLoginTime) && plugin.getConfigs().getBoolean("Kick last logged", true)) {
				kick = i;
			} else if ((iLoginTime != 0) && (iLoginTime < kickLoginTime) && !plugin.getConfigs().getBoolean("Kick last logged", true)) {
				kick = i;
			}
		}
		options.get(kick).kickPlayer(plugin.getConfigs().getString("custom kick message", "Server is full. A VIP signed in."));
		event.setResult(Result.ALLOWED);
		return;
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {
		Player player = event.getPlayer();
		plugin.logintimes.set("DO NOT EDIT -- Login times." + player.getName(), player.getWorld().getFullTime());
		try {
			plugin.logintimes.save(plugin.loginfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}

	@EventHandler
	public void onPlayerQuit(final PlayerQuitEvent event) {
		Player player = event.getPlayer();
		plugin.logintimes.set("DO NOT EDIT -- Login times." + player.getName(), null);
		try {
			plugin.logintimes.save(plugin.loginfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}
}