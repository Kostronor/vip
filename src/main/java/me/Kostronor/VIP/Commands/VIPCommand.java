package me.Kostronor.VIP.Commands;

import me.Kostronor.VIP.VIP;
import me.Kostronor.VIP.Permissions.VIPPermissions;

import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class VIPCommand implements CommandExecutor {
	private final VIP plugin;
	public VIPPermissions permissions;

	public VIPCommand(VIP plugin){
		this.plugin = plugin;
		permissions=plugin.permissions;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		boolean console = false;
		Player player = null;
		if (!(sender instanceof Player)){
			console = true;
		}else{
			player = (Player) sender;
		}
		if (args.length==1){
			if(args[0].equalsIgnoreCase("enable")){
				if(console || permissions.canEnable(player)){
					if(!plugin.getConfigs().getBoolean("enabled", false)){
						plugin.getConfigs().set("enabled", true);
						plugin.saveConfig();
						plugin.getLogger().info("VIP enabled");
						sender.sendMessage("VIP enabled");
					}else{
						sender.sendMessage("VIP is already enabled.");
					}
				}
				else{
					sender.sendMessage("You do not have permission to do that.");
				}
				return true;
			}
			else if(args[0].equalsIgnoreCase("disable")){
				if(console || permissions.canDisable(player)){
					if(plugin.getConfigs().getBoolean("enabled", true)){
						plugin.getConfigs().set("enabled", false);
						plugin.saveConfig();
						plugin.getLogger().info("VIP disabled");
						sender.sendMessage("VIP disabled");
					}else{
						sender.sendMessage("VIP is already disabled.");
					}
				}
				else{
					sender.sendMessage("You do not have permission to do that.");
				}
				return true;
			}
			else if(args[0].equalsIgnoreCase("kick")){
				if(console || permissions.canToggleKick(player)){
					plugin.getConfigs().set("Kick last logged", !plugin.getConfigs().getBoolean("Kick last logged", false));
					plugin.saveConfig();
					sender.sendMessage("\"Kick last logged\" is now set to: " + plugin.getConfigs().getString("Kick last logged"));
				}
				else{
					sender.sendMessage("You do not have permission to do that.");
				}
				return true;
			}
			else if(args[0].equalsIgnoreCase("permissions")){
				if(console || player.isOp()){
					plugin.getConfigs().set("Use permissions for VIP list", !plugin.getConfigs().getBoolean("Use permissions for VIP list", false));
					plugin.saveConfig();
					sender.sendMessage("\"Use permissions for VIP list\" is now set to: " + plugin.getConfigs().getString("Use permissions for VIP list"));
				}
				else{
					sender.sendMessage("You do not have permission to do that.");
				}
				return true;
			}
		}
		if(!plugin.getConfigs().getBoolean("enabled", false)){
			sender.sendMessage("VIP is not enabled!");
			return true;
		}
		if (args.length<2){
			return false;
		}
		else if(!(args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove"))){
			return false;
		}
		String addRemove = args[1];
		String action = args[0];
		if(action.equalsIgnoreCase("add")){
			if( !permissions.canAddPlayer(player)){
				if(!console){
					sender.sendMessage("You do not have permission to do that.");
					return true;
				}
			} 
			if(!plugin.getConfigs().getBoolean("Use permissions for VIP list", false)){

				int priority = 1;
				if(args.length==3){
					try{
						priority = Integer.parseInt(args[2].trim());
					}
					catch (NumberFormatException e){
						return false;
					}
				}
				if(priority<1){
					sender.sendMessage("Priority must be positive");
					return true;
				}
				plugin.getConfigs().set("VIPs." + addRemove, priority);
				plugin.saveConfig();
				sender.sendMessage(addRemove + " is now a VIP with priority " + priority);
			}
			else{
				sender.sendMessage("VIP is using permissions, give them their permission!");
			}
			return true;
		}
		else if(action.equalsIgnoreCase("remove")){
			if(!permissions.canRemovePlayer(player)){
				if(!console){
					sender.sendMessage("You do not have permission to do that.");
					return true;
				}
			}
			if(!plugin.getConfigs().getBoolean("Use permissions for VIP list", false)){
				plugin.getConfigs().set("VIPs." + addRemove, null);
				plugin.saveConfig();
			}			else{
				sender.sendMessage("VIP is using permissions, give them their permission!");
				return true;
			}
			sender.sendMessage(addRemove + " is no longer a VIP.");
		}
		return true;
	}

}
